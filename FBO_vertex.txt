#version 140

uniform float u_time;
uniform mat4 u_modelViewMatrix;
uniform int u_debugInt;


out vec2 v_texCoords;
out vec4 v_surfaceColor;
out vec3 v_vertexPosition;
out vec4 v_screenPosition;

in vec4 a_vertexColor;
in vec2 a_vertexTexCoords;
in vec3 a_vertexPosition;


//Randomize Code Found Here:
//http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/

highp float rand(vec2 co)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}


void main()
{
	vec4 temporary = u_modelViewMatrix * vec4(a_vertexPosition, 1.0);
	v_screenPosition = temporary;
	v_texCoords = a_vertexTexCoords;
	v_vertexPosition = a_vertexPosition;
	v_surfaceColor = a_vertexColor;

	if( u_debugInt == 1)
		v_surfaceColor = vec4(0.2,0.2,1.0,1.0);

	gl_Position = v_screenPosition;
}